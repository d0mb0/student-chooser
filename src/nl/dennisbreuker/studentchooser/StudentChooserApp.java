/*
 * This file was created to facilitate lessons at HBO-ICT@HvA.
 */
package nl.dennisbreuker.studentchooser;

import java.io.File;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.Separator;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author dennis
 */
public class StudentChooserApp extends Application {

    private static final String VERSION = "0.2";
    private static final String TITLE = "Choose a student v" + VERSION;
    private static final String ALWAYS_VARY = "Always vary";
    private static final String CHOOSE = "Choose";
    private static final String HELP = "Help";
    private static final String SEMICOLON = ";";
    private static final String COMMA = ",";
    private static final String NEWLINE = "\\n";
    private static final String SPACE = "_";
    private static final String REMOVE_ALL = "Remove all";
    private static final String IMPORT_STUDENTS = "Import students";
    private static final String ABENT = "Absent";
    private static final String ADD_STUDENT = "Add student";
    private static final String STUDENTS_PRESENT = " students present";
    private static final int FONT_SIZE = 30;
    private static final String HELP_TEXT
            = "<body style='background : rgb(243,243,243); font-family:System; font-style:Regular; font-size:13;'>"
            + "<ul>"
            + "<li><b>Remove all</b>: removes all students from the application.</li>"
            + "<li><b>Import students</b>: imports students from a text file.<br />"
            + "The separator (separating the names in the file) can be chosen:"
            + "Space, New line, Comma, Semicolon.</li>"
            + "<li><b>Add student</b>: adds a student temporarily to the application.</li>"
            + "<li><b>Absent</b>: removes the randomly chosen student from the application (not from the file) and chooses a new random student.</li>"
            + "<li><b>Choose</b>: choose a random student.<br />If 'always vary' is checked, the next random student will differ from the previous.</li>"
            + "</ul></body>";
    private Chooser<String> chooser;
    private String chosenStudent = null;
    private Button btnRandom;
    private CheckBox cbAlwaysVary;
    private CheckBox cbSpace, cbNewLine, cbComma, cbSemicolon;
    private Label lblSpace, lblNewLine, lblComma, lblSemicolon;
    private Label lblTotalPresent, lblChosenStudent;
    private VBox vbSpace, vbNewLine, vbComma, vbSemicolon;
    private HBox hbDelimiterCheckBoxes;
    private TextField tfAddStudent;

    @Override
    public void start(Stage stage) {
        chooser = new Chooser();

        // Menu
        MenuBar menuBar = new MenuBar();

        // File
        Menu menuHelp = new Menu(HELP);
        
        // Ugly workaround to make the menu clickable (instead of a menu item)
        menuHelp.showingProperty().addListener(new ChangeListener<Boolean>() { // NOPMD
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean oldValue, Boolean newValue) {
                if (newValue.booleanValue()) {
                    System.out.println("Showing|Clicked");
                    // TODO show help text. Moet eigenlijk in een Popup class
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle(HELP);
                    alert.setHeaderText(null);
                    WebView webView = new WebView();
                    webView.getEngine().loadContent(HELP_TEXT);
                    webView.setPrefSize(480, 180);
                    alert.getDialogPane().setContent(webView);;
                    alert.showAndWait();
                } else {
                    System.out.println("Not showing|Clicked again");
                }
            }

        });
        
        // Ugly fake item, otherwise the clickListener won't work
        MenuItem m = new MenuItem();
        menuHelp.getItems().add(m);

        menuBar.getMenus().add(menuHelp);

        // total present
        lblTotalPresent = new Label(chooser.size() + STUDENTS_PRESENT);
        lblTotalPresent.setMaxWidth(Double.MAX_VALUE);
        lblTotalPresent.setAlignment(Pos.CENTER);

        // remove all
        Button btnRemoveAll = new Button(REMOVE_ALL);
        btnRemoveAll.setMaxWidth(Double.MAX_VALUE);
        btnRemoveAll.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                chooser.removeAll();
                lblTotalPresent.setText(chooser.size() + STUDENTS_PRESENT);
                lblChosenStudent.setText("");
                chosenStudent = null; // TODO wil ik dat?
            }
        });

        Separator separator1 = new Separator();

        // import students
        Button btnImport = new Button(IMPORT_STUDENTS);
        btnImport.setMaxWidth(Double.MAX_VALUE);
        btnImport.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FileChooser fileChooser = new FileChooser();
                File file = fileChooser.showOpenDialog(stage); // TODO save
                String delimiters = createDelimiters();
                chooser.importItems(file, delimiters);
                lblTotalPresent.setText(chooser.size() + STUDENTS_PRESENT);
                lblChosenStudent.setText("");
                chosenStudent = null; // TODO wil ik dat?
            }
        });

        // TODO: save last chosen values (also directory)
        cbSpace = new CheckBox();
        lblSpace = new Label(SPACE);
        vbSpace = new VBox(5.0, lblSpace, cbSpace);

        cbNewLine = new CheckBox();
        cbNewLine.setSelected(true);
        lblNewLine = new Label(NEWLINE);
        vbNewLine = new VBox(5.0, lblNewLine, cbNewLine);

        cbComma = new CheckBox();
        lblComma = new Label(COMMA);
        vbComma = new VBox(5.0, lblComma, cbComma);

        cbSemicolon = new CheckBox();
        lblSemicolon = new Label(SEMICOLON);
        vbSemicolon = new VBox(5.0, lblSemicolon, cbSemicolon);

        hbDelimiterCheckBoxes = new HBox(5.0, vbSpace, vbNewLine, vbComma, vbSemicolon);

        // add student
        Button btnAdd = new Button(ADD_STUDENT);
        btnAdd.setMaxWidth(Double.MAX_VALUE);
        btnAdd.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String student = tfAddStudent.getText();
                if (!student.isEmpty()) {
                    chooser.addItem(student);
                    // TODO test if already present
                    tfAddStudent.clear();
                }
                lblTotalPresent.setText(chooser.size() + STUDENTS_PRESENT);
                lblChosenStudent.setText("");
                chosenStudent = null; // TODO wil ik dat?
            }
        });
        tfAddStudent = new TextField();

        Separator separator2 = new Separator();

        // remove student
        Button btnRemove = new Button();
        btnRemove.setMaxWidth(Double.MAX_VALUE);
        btnRemove.setText(ABENT);
        btnRemove.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (chosenStudent != null) {
                    chooser.removeItem(chosenStudent);
                    lblTotalPresent.setText(chooser.size() + STUDENTS_PRESENT);
                    btnRandom.fire();
                }
            }
        });

        // random student
        btnRandom = new Button();
        btnRandom.setMaxWidth(Double.MAX_VALUE);
        btnRandom.setText(CHOOSE);
        btnRandom.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String student;
                do {
                    student = chooser.chooseRandomly();
                } while (cbAlwaysVary.isSelected()
                        && chooser.size() > 1
                        && chosenStudent != null
                        && chosenStudent.equals(student));
                chosenStudent = student;
                lblChosenStudent.setText(chosenStudent);
            }
        });
        cbAlwaysVary = new CheckBox(ALWAYS_VARY);
        cbAlwaysVary.setSelected(true); // TODO save

        Separator separator3 = new Separator();

        // chosen student
        lblChosenStudent = new Label();
        lblChosenStudent.setMaxWidth(Double.MAX_VALUE);
        lblChosenStudent.setAlignment(Pos.CENTER);
        lblChosenStudent.setFont(new Font(FONT_SIZE));

        // --------------------------------------------------------------------
        GridPane root = new GridPane();
        root.setHgap(10);
        root.setVgap(10);
        ColumnConstraints column = new ColumnConstraints();
        column.setPercentWidth(30);
        root.getColumnConstraints().add(column);

        column = new ColumnConstraints();
        column.setPercentWidth(70);
        root.getColumnConstraints().add(column);

        int row = 0;

//        root.getChildren().addAll(menuBar);
        root.add(menuBar, 0, row++, 3, 1);

        root.add(lblTotalPresent, 0, row++, 2, 1);

        root.add(btnRemoveAll, 0, row++);

        root.add(separator1, 0, row++, 2, 1);

//        root.add(cbSpace, 1, row++);
//        root.add(btnImport, 0, row);
//        root.add(cbNewLine, 1, row++);
//        root.add(cbComma, 1, row++);
//        root.add(cbSemicolon, 1, row++);
//        root.add(lblSpace, 0, row);
//        root.add(lblNewLine, 1, row);
//        root.add(lblComma, 2, row);
//        root.add(lblSemicolon, 3, row++);
// 
//        root.add(cbSpace, 0, row);
//        root.add(cbNewLine, 1, row);
//        root.add(cbComma, 2, row);
//        root.add(cbSemicolon, 3, row++);
//        root.add(vbSpace, 0, row);
//        root.add(vbNewLine, 1, row);
//        root.add(vbComma, 2, row);
//        root.add(vbSemicolon, 3, row++);
        root.add(btnImport, 0, row);
        root.add(hbDelimiterCheckBoxes, 1, row++);

        root.add(btnAdd, 0, row);
        root.add(tfAddStudent, 1, row++);

        root.add(separator2, 0, row++, 2, 1);

        root.add(btnRemove, 0, row++);

        root.add(btnRandom, 0, row);
        root.add(cbAlwaysVary, 1, row++);

        root.add(separator3, 0, row++, 2, 1);

        root.add(lblChosenStudent, 0, row++, 2, 1);

        Scene scene = new Scene(root, 500, 350);

        stage.setTitle(TITLE);
        stage.setScene(scene);
        stage.show();
    }

    private String createDelimiters() {
        StringBuilder s = new StringBuilder("[");
        if (cbSpace.isSelected()) {
            s.append(SPACE);
        }
        if (cbNewLine.isSelected()) {
            s.append(NEWLINE);
        }
        if (cbComma.isSelected()) {
            s.append(COMMA);
        }
        if (cbSemicolon.isSelected()) {
            s.append(SEMICOLON);
        }
        s.append("]");
        return s.toString();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
