/*
 * This file was created to facilitate lessons at HBO-ICT@HvA.
 */
package nl.dennisbreuker.studentchooser;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author dennis
 */
public class Chooser<Item> {

    final private Set<Item> items;
    final private Random rand;

    public Chooser() {
        this(new HashSet<>());
    }
    
    public Chooser(Set<Item> items) {
        this.items = items;
        rand = new Random();
    }
    
    public int size() {
        return items.size();
    }
    
    public boolean addItem(Item item) {
        return items.add(item);
    }

    public boolean removeItem(Item item) {
        return items.remove(item);
    }
    
    public Item chooseRandomly() {
        if (items.isEmpty()) {
            return null;
        }
        int randIndex = rand.nextInt(items.size());
        return (Item) items.toArray()[randIndex];
    }
    
    public boolean importItems(File file, String delimiters) {
        // TODO this is strings only, so check on item being a string
        // TODO maybe subclass with <Item instanceof String>?
        if (file == null) {
            // TODO report error?
            return false;
        }
        try {
            Scanner inFile = new Scanner(file).useDelimiter(delimiters);
            while (inFile.hasNext()) {
                String token = inFile.next();
                if (!token.isEmpty()) {
                    items.add((Item)token); // TODO check on identical names
                }
            }
            inFile.close();
        } catch (FileNotFoundException ex) {
            // TODO report error
            return false;
        }
        return false;
    }
    
    public void removeAll() {
        items.clear();
    }
}
