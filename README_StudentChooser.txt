README StudentChooser

- Remove All
	Verwijdert alle studenten uit de lijst (dus niet uit een bestand)

- Import students
	Importeert studenten uit tekstbestand. Je kan de delimiter opgeven: spatie, newline, komma en puntkomma. Maak dus vooraf een tekstbestand van je klas

- Add student
	Handmatig extra student toevoegen die eenmalig aanwezig is

- Absent
	Gekozen student is niet aanwezig, wordt uit de lijst gehaald (dus niet uit een bestand)

- Choose
	Kiest random student. Als vinkje 'Always vary' aan staat wordt nooit twee keer achter elkaar dezelfde student gekozen